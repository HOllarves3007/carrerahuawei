<?php
App::uses('Life', 'Model');

/**
 * Life Test Case
 */
class LifeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.life',
		'app.user',
		'app.score'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Life = ClassRegistry::init('Life');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Life);

		parent::tearDown();
	}

}

<div class="lives index">
	<h2><?php echo __('Lives'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('quantity'); ?></th>
			<th><?php echo $this->Paginator->sort('given'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($lives as $life): ?>
	<tr>
		<td><?php echo h($life['Life']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($life['User']['name'], array('controller' => 'users', 'action' => 'view', $life['User']['id'])); ?>
		</td>
		<td><?php echo h($life['Life']['quantity']); ?>&nbsp;</td>
		<td><?php echo h($life['Life']['given']); ?>&nbsp;</td>
		<td><?php echo h($life['Life']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $life['Life']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $life['Life']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $life['Life']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $life['Life']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Life'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>

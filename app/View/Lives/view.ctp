<div class="lives view">
<h2><?php echo __('Life'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($life['Life']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($life['User']['name'], array('controller' => 'users', 'action' => 'view', $life['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantity'); ?></dt>
		<dd>
			<?php echo h($life['Life']['quantity']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Given'); ?></dt>
		<dd>
			<?php echo h($life['Life']['given']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($life['Life']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Life'), array('action' => 'edit', $life['Life']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Life'), array('action' => 'delete', $life['Life']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $life['Life']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Lives'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Life'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>

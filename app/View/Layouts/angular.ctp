<!DOCTYPE html>

<html ng-app="liptonApp">

  <head>
  <title>Escápate con Lipton </title>
  <?php

    /*<link rel="stylesheet" href="/css/bootstrap.min.css">

    <link rel="stylesheet" href="/css/font-awesome.css">

    <link rel="stylesheet" href="/css/liptonFrontSprites.css">

    <link rel="stylesheet" href="/css/stylesheet.css">*/

    $this->Html->css([ "/css/bootstrap.min","/css/font-awesome","/css/liptonFrontSprites","/css/stylesheet"], ['inline' => false]);

    echo $this->fetch('meta');
    if($redirection === FALSE)
      echo $this->fetch('css');

    ?>

    <meta charset="utf-8">

    <meta charset="utf-8">

    <link rel="shortcut icon" href="/favicon.ico">

    <meta property="og:title" content="Escápate con Lipton">

    <meta property="og:site_name">

    <meta property="og:url">

    <meta property="og:description">

    <meta property="og:image">

    <meta property="fb:app_id" content="129592227389265">

    <meta name="twitter:card" content="photo">

    <meta name="twitter:title">

    <meta name="twitter:description">

    <meta name="twitter:image">

    <meta name="twitter:image:width" content="1200">

    <meta name="twitter:image:heigth" content="630">

  </head>

  <body ng-controller="mainController">

    <div class="mainContainer">

      <!-- MAIN CONTENT AND INJECTED VIEWS-->

      <div class="gameContent">

        <!-- angular templating-->
        <?php 
          if($redirection !== FALSE)
            echo $this->fetch('content'); 
        ?>
        <!-- this is where content will be injected-->

        <div ng-view=""></div>

      </div>

    </div>

    <?php

      /*

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>

      <script src="/js/jquerySnow.js"></script>

      <script src="//platform.twitter.com/widgets.js"></script>

      <script src="/js/lodash.js"></script>

      <script src="/js/angular/angular.js"></script>

      <script src="/js/ui-bootstrap-tpls-0.9.0.min.js"></script>

      <script src="/js/angular/angular-route.min.js"></script>

      <script src="/js/angular/angular-canvasloader.js"></script>

      <script src="/js/angular/ngFacebook.js"></script>

      <script src="js/script.js"> </script>

      */


      if($redirection === FALSE)
      echo $this->Html->script([ "https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js", "jquerySnow.js", "//platform.twitter.com/widgets.js", "lodash.js", "angular/angular.js", "ui-bootstrap-tpls-0.9.0.min.js", "angular/angular-route.min.js", "angular/angular-canvasloader.js", "angular/ngFacebook.js", "moment.min.js", "angular/angular-moment.min.js", "script.js"]);



    ?>

    <script>

      window.fbAsyncInit = function() {

      };

      (function(d, s, id){

      var js, fjs = d.getElementsByTagName(s)[0];

      if (d.getElementById(id)) {return;}

      js = d.createElement(s); js.id = id;

      js.src = "//connect.facebook.net/en_US/sdk.js";

      fjs.parentNode.insertBefore(js, fjs);

      }(document, 'script', 'facebook-jssdk'));

    </script>

  </body>

</html>




gamePlay = (function(window){



    var gameObject, phaserGame, preload, create, update, gameState, gameDefaultState, worldWidth, worldHeight,    // GameObjects



        runner, runnerTween, liptonBackgroundSpeed, liptonSkySpeed, liptonCloudSpeed,             // SpriteObjects



        groups, preloadBar, background_7, background_6, background_5, background_4, background_3, background_2, background_1, backgroundEnd,                       // Collectibles



        generateCollectibles , triggerLeft, triggerRight                                           //  Custom Methods



        ,up,left,down,right,gamekey, starKey,leftButton,rightButton, leftKey, rightKey



        ,cursor, score, scoreStar, starCounter, photoCircle, expectedInput,



        giftCircle, giftIcon, giftScore, textStyle, timer, newTimer, milliseconds, seconds, minutes, secondHolder,                   // score bar



        multiplications, multiply, gameHasStarted, text,



        nextEnemyAt, enemyDelay, time, gameSocket, gameEnd, gameEndDelay, gameEnding, backgroundLoopCounter, runnerTweened, ngScope;







    gameObject = {};







    var style = { font: "22px Arial", fill: "#FFFFFF", align: "center" };



    var style2 = { font: "18px Arial", fill: "#1B2F40", align: "center", fontWeight: "bold"};







    preload = function(){




        secondHolder = 0;


        milliseconds = 0;



        seconds = 0;



        minutes = 0;







        phaserGame.stage.backgroundColor =  "#0C555D";











        this.gameTimer = this.game.time.create(false);



        phaserGame.load.bitmapFont("arial", "fonts/arial/arial.png", "fonts/arial/arial.fnt");











        preloadBar = phaserGame.add.sprite(phaserGame.width / 2 - 100, phaserGame.height / 2, 'preloaderBar');



        phaserGame.load.setPreloadSprite(preloadBar);



        phaserGame.load.image('stopWatch', "img/end/stopWatch.png")



        phaserGame.load.image("liptonBackgroundCity", "img/liptonBackgroundCity.png");



        phaserGame.load.image("liptonBackgroundSuburbs", "img/liptonBackgroundSuburbs.png");



        phaserGame.load.image("liptonBackgroundCityForest", "img/liptonBackgroundCityForest.png");



        phaserGame.load.image("liptonBackgroundCityForest2", "img/liptonBackgroundCityForest2.png");



        phaserGame.load.image("liptonBackgroundForestCity", "img/liptonBackgroundForestCity.png");



        phaserGame.load.image("liptonBackgroundForest", "img/liptonBackgroundForest.png");



        phaserGame.load.image("liptonBackgroundForest2", "img/liptonBackgroundForest2.png");



        phaserGame.load.image("liptonBackgroundEnd", "img/liptonBackgroundEnd.png");



        phaserGame.load.image('liptonHUD', "img/liptonHUD.png");



        phaserGame.load.image('liptonSky', "img/liptonSky.png");



        phaserGame.load.atlasJSONArray("runnerSprite", "img/assets/liptonRunnerSprites.png", "img/assets/liptonRunnerSprites.json");



        







    }







    var max = 0;



    var front_emitter;



    var mid_emitter;



    var back_emitter;



    var update_interval = 4 * 60;



    var i = 0;







    multiply = function(factor1, factor2)



    {



        if (!multiplications[factor1]) {



            multiplications[factor1] = [];



        };



        if (!multiplications[factor1][factor2]) {



            multiplications[factor1][factor2] = factor1 * factor2;



        };



        return multiplications[factor1][factor2];



    }







    create = function(){



        var leftBound, topBound, runner_ani, min_quantity, max_quantity;



        preloadBar.visible = false;



        leftBound = 0; // 2600 - phaserGame.width;



        topBound  = 0;



        text = 0;







        phaserGame.time.events.loop(Phaser.Timer.SECOND, updateCounter, this);







        enemyDelay = 500;



        nextEnemyAt = 0;







        backgroundLoopCounter = 0;



        worldWidth = 10000;



        worldWidth = window.innerWidth;



        worldHeight = 1200;



        worldHeight = window.innerHeight;







        multiplications = {};







        gameEnd = phaserGame.time.now + 60000;



        gameEndDelay = 5000;



        gameEnding = 0;



        runnerTweened = false;







        up = false;



        down = false;



        left = false;



        right = false;







        // initialize animations



        runner_ani = [



        "liptonRunner0.png",



        "liptonRunner1.png",



        "liptonRunner2.png",



        "liptonRunner3.png",



        "liptonRunner4.png",



        "liptonRunner5.png",



        "liptonRunner6.png",



        "liptonRunner7.png",



        "liptonRunner8.png",



        "liptonRunner9.png",



        "liptonRunner10.png",



        "liptonRunner11.png"



        ];











        phaserGame.physics.startSystem(Phaser.Physics.ARCADE);







        phaserGame.world.setBounds(leftBound, topBound, worldWidth, worldHeight, style);



        phaserGame.scale.scaleMode = Phaser.ScaleManager.RESIZE;







        phaserGame.camera.width = window.innerWidth;



        phaserGame.camera.height = window.innerHeight;







        



        //liptonBackgroundSpeed = -10;



        liptonBackgroundSpeed = 0;



        liptonSkySpeed = -15;



        liptonCloudSpeed = 0;







        //stopWatch = phaserGame.add.tileSprite(0, phaserGame.height 0,0,0, 'stopWatch');





        liptonSky = phaserGame.add.tileSprite(0, phaserGame.height -700, 9000, 800, 'liptonSky');



        liptonHUD = phaserGame.add.tileSprite(0, phaserGame.height -700, 90000, 800, 'liptonHUD');



        backgroundEnd = phaserGame.add.tileSprite(0, phaserGame.height -700, 9000, 800, 'liptonBackgroundEnd');



        background_1 = phaserGame.add.tileSprite(0, phaserGame.height -700, 9000, 800, 'liptonBackgroundForest2');



        background_2 = phaserGame.add.tileSprite(0, phaserGame.height -700, 9000, 800, 'liptonBackgroundForest');



        background_3 = phaserGame.add.tileSprite(0, phaserGame.height -700, 9000, 800, 'liptonBackgroundForestCity');



        background_4 = phaserGame.add.tileSprite(0, phaserGame.height -700, 9000, 800, 'liptonBackgroundCityForest2');



        background_5 = phaserGame.add.tileSprite(0, phaserGame.height -700, 9000, 800, 'liptonBackgroundCityForest');



        background_6 = phaserGame.add.tileSprite(0, phaserGame.height -700, 9000, 800, 'liptonBackgroundSuburbs');



        background_7 = phaserGame.add.tileSprite(0, phaserGame.height -700, 9000, 800, 'liptonBackgroundCity');



        phaserGame.add.sprite(5, 5, 'stopWatch');



        background_4.fixedToCamera = true;



        background_3.fixedToCamera = true;



        background_2.fixedToCamera = true;



        background_1.fixedToCamera = true;



        backgroundEnd.fixedToCamera = true;



        background_5.fixedToCamera = true;



        background_6.fixedToCamera = true;



        liptonHUD.fixedToCamera = true;



        liptonSky.fixedToCamera = true;







        







        runner = phaserGame.add.sprite(phaserGame.world.centerX, 500, 'runnerSprite', 'liptonRunner0.png');



        runner.anchor.setTo(0.5, 0.5);



        runner.animations.add("runForTea", runner_ani, 15, true);



        runner.play("runForTea");











        /*runnerTween = phaserGame.make.sprite(phaserGame.world.centerX, phaserGame.world.centerY, 'sprites', '../img/SpriteSync/liptonRunner0.png');



        runnerTween.anchor.setTo(0.5, 0.5);



        runnerTween.animations.add("runForTea", runner_ani, 12, true);*/







        phaserGame.physics.enable(runner, Phaser.Physics.ARCADE);



        //runner.body.collideWorldBounds = true;







        gameDefaultState = {



            runnerVelocity: 200,



            runnerBagMedium: 500,



            runnerBagFull: 1000,



            velocityTimeoutAmount: 5000,



            multiplierTimeoutAmount: 5000,



            collectiblesVelocity: -350



        }







        gameState = {



            velocityMultiplier: 2,



            pointsMultiplier: 1,



            velocityTimeoutId: null,



            multiplierTimeoutId: null,



            runnerState: true,



            runnerSpeed: null,



            minutesSwitch: false



        };











        score = phaserGame.add.text(490, 47, gameState.pointsAccumulated,style);



        //photoCircle = phaserGame.add.sprite(560,42,'whiteCircle');



        



        score.fixedToCamera = true;



        //photoCircle.fixedToCamera = true;







        phaserGame.camera.follow(runner, Phaser.Camera.FOLLOW_PLATFORMER);



        phaserGame.camera.deadzone = new Phaser.Rectangle(runner.width / 2, runner.height / 2, window.innerWidth - 238, window.innerHeight - 100);







        convertZone = phaserGame.add.sprite(0,0);



        convertZone.width = window.innerWidth;



        convertZone.height = window.innerHeight;



        convertZone.fixedToCamera = true;



        phaserGame.physics.enable(convertZone, Phaser.Physics.ARCADE);







        timer = phaserGame.add.bitmapText(50, 15, 'arial', '00:00', 24);







    }







    velocityTimeoutCallback = function(){



        gameState.velocityMultiplier = 2;






    }







    generateCollectibles = function(_group, _groupname, _quantity){



        var collectible, extraname, colors;



        if(typeof groups[_group] == "string"){



            groups[_group] = phaserGame.add.group(phaserGame.world, _groupname, true, true, Phaser.Physics.ARCADE);



            groups[_group].enableBody = true;



        }else



        _groupname = groups[_group].name;







        extraname = "";



        groups[_group].fixedToCamera = false;







        groups[_group].createMultiple(_quantity, "sprites", _groupname + "0");



        groups[_group].setAll('anchor.x', 0.5);



        groups[_group].setAll('anchor.y', 0.5);



        groups[_group].setAll('outOfBoundsKill', true);



        groups[_group].setAll('checkWorldBounds', true);











    }







    function updateCounter() {



        seconds++;

        secondHolder++;

        if(secondHolder >= 60){
            secondHolder = 0;
        }

        minutes = Math.floor((seconds*1000) / 60000) % 60;


        if (secondHolder < 10)
                secondHolder = '0' + secondHolder;

        if(minutes < 10 ){

            minutes = '0' + minutes;      
        }

/*        minutes = '0' + minutes;

        if (minutes >= 10){
            minutes = minutes;
        }
*/
        timer.setText(minutes + ':' +  secondHolder);





    }







    getRandomInt = function(min, max) {



      return Math.floor(multiply(Math.random(), (max - min)) + min);



    } 











    triggerLeft = function(){



        if(cursor.left.isDown){






            //FIRST RUN



            if(cursor.left.isDown && typeof expectedInput == 'undefined'){  



                //runner.body.velocity.x = 2 + multiply(gameDefaultState.runnerVelocity, gameState.velocityMultiplier);



                liptonBackgroundSpeed -= 35;








            }



            //MISTAKE



            if(cursor.left.isDown && expectedInput == rightKey){



                //runner.body.velocity.x = 2 - multiply(gameDefaultState.runnerVelocity, gameState.velocityMultiplier);



                liptonBackgroundSpeed += 30;






            }



            //SUCCESS



            if(cursor.left.isDown && expectedInput == leftKey){



                //runner.body.velocity.x = 2 + multiply(gameDefaultState.runnerVelocity, gameState.velocityMultiplier);



                liptonBackgroundSpeed -= 35;








            }



        }



    };



    triggerRight = function(){



        if(cursor.right.isDown){






            //FIRST RUN



            if(cursor.right.isDown && typeof expectedInput == 'undefined'){



                //runner.body.velocity.x = 2 + multiply(gameDefaultState.runnerVelocity, gameState.velocityMultiplier);



                liptonBackgroundSpeed -= 35;








            }



            //MISTAKE



            if(cursor.right.isDown && expectedInput == leftKey){



                //runner.body.velocity.x = 2 - multiply(gameDefaultState.runnerVelocity, gameState.velocityMultiplier);



                liptonBackgroundSpeed += 30;






            }



            //SUCCESS



            if(cursor.right.isDown && expectedInput == rightKey){



                //runner.body.velocity.x = 2 + multiply(gameDefaultState.runnerVelocity, gameState.velocityMultiplier);



                liptonBackgroundSpeed -= 35;








            }



        }



    };







    update = function(){





    	cursor = phaserGame.input.keyboard.createCursorKeys();



    	leftButton = cursor.left.onDown;



    	rightButton = cursor.right.onDown;






        if (backgroundEnd.visible == true && background_1.visible == false) {



            var toX, toY;



            if (!runnerTweened) {










                toX = phaserGame.camera.width / 2;



                //toY = phaserGame.camera.height;



                //runner.fixedToCamera = true;



                runner.events.onKilled.addOnce(function(){



                    phaserGame.paused = true;



                    window.location.href = "#end";



                }, runner);







                phaserGame.add.tween(runner).to({ x: toX, y: toY }, 2000, Phaser.Easing.Quadratic.Out, true)



                .onComplete.addOnce(function(){



                    //gameSocket.emit("gameEnd",{"time": timer.text});



                    runner.body.velocity.setTo(0, 0);



                    runner.body.velocity.x = 1000;



                    runner.body.collideWorldBounds = false;



                    runner.checkWorldBounds = true;



                    runner.outOfBoundsKill = true;



                    //ngScope = timer.text;

                    var toSeconds = seconds * 1000;

                    if(minutes == 0){
                        ngScope.$emit('endGame',{"time": toSeconds});
                    }
                    else{
                        for(var i = 0; i == minutes; i++){
                            toSeconds += 60000;
                        }
                        ngScope.$emit('endGame',{"time": toSeconds});
                    }





                    //ngScope






                }, this);



                runnerTweened = true;










            }



        } else{




            if(gameState.runnerState == true){



                if(liptonBackgroundSpeed <= -1){



                    backgroundLoopCounter += 1;



                    liptonBackgroundSpeed += 2;



                    gameState.runnerSpeed = 'NS';



                }



                if(liptonBackgroundSpeed <= -500 && liptonBackgroundSpeed >= -1000){



                    backgroundLoopCounter += 4;



                    liptonBackgroundSpeed += 2;



                    gameState.runnerSpeed ='PS';







                }



                if(liptonBackgroundSpeed <= -1000 && liptonBackgroundSpeed >= -1500){



                    backgroundLoopCounter += 6;



                    liptonBackgroundSpeed += 2;



                    gameState.runnerSpeed = 'HS';



                }



                if(liptonBackgroundSpeed <= -1500 && liptonBackgroundSpeed >= -2000){



                    backgroundLoopCounter += 8;



                    liptonBackgroundSpeed += 2;



                    gameState.runnerSpeed = 'ES';



                }



                if(liptonBackgroundSpeed <= -2000){



                    backgroundLoopCounter += 10;



                    liptonBackgroundSpeed += 2;



                    gameState.runnerSpeed = "LightSpeed";



                }



                if(backgroundLoopCounter >= 0){



                    background_7.visible = true;



                    background_6.visible = false;



                    background_5.visible = false;



                    background_4.visible = false;



                    background_3.visible = false;



                    background_2.visible = false;



                    background_1.visible = false;



                    backgroundEnd.visible = false;



                }



                if(backgroundLoopCounter >= 2500){



                    background_6.visible = true;



                }



                if(backgroundLoopCounter >= 2520){



                    background_7.visible = false;



                }



                if(backgroundLoopCounter >= 3000){



                    background_5.visible = true;



                }



                if(backgroundLoopCounter >= 3020){



                    background_6.visible = false;



                }



                if(backgroundLoopCounter >= 4000){



                    background_4.visible = true;



                }



                if(backgroundLoopCounter >= 4020){



                    background_5.visible = false;



                }



                if(backgroundLoopCounter >= 5500){







                    background_3.visible = true;



                }                



                if(backgroundLoopCounter >= 5550){



                    background_4.visible = false;



                }



                if(backgroundLoopCounter >= 7000){



                    background_2.visible = true;



                }



                if(backgroundLoopCounter >= 7020){



                    background_3.visible = false;



                }



                if(backgroundLoopCounter >= 8000){



                    background_1.visible = true;



                }



                if(backgroundLoopCounter >= 8020){



                    background_2.visible = false;



                }



                if(backgroundLoopCounter >= 10500){



                    backgroundEnd.visible = true;



                }



                if(backgroundLoopCounter >= 10520){



                    background_1.visible = false;



                }



            }





            leftButton.add(triggerLeft, this);



            rightButton.add(triggerRight,this);


/*
            cursor.left.onDown.add(triggerLeft, this);

            cursor.right.onDown.add(triggerRight, this);*/







            rightKey = 'right';



            leftKey = 'left';








            backgroundEnd.autoScroll(liptonBackgroundSpeed, 0);



            background_1.autoScroll(liptonBackgroundSpeed, 0);



            background_2.autoScroll(liptonBackgroundSpeed, 0);



            background_3.autoScroll(liptonBackgroundSpeed, 0);



            background_4.autoScroll(liptonBackgroundSpeed, 0);



            background_5.autoScroll(liptonBackgroundSpeed, 0);



            background_6.autoScroll(liptonBackgroundSpeed, 0);



            background_7.autoScroll(liptonBackgroundSpeed, 0);



            liptonSky.autoScroll(liptonSkySpeed, 0);







            if(leftButton.isDown){



                phaserGame.input.keyboard.onUpCallback = function(e){

                    if(e.keyCode == leftButton){


                        //FIRST RUN

                        if(typeof expectedInput == 'undefined'){  

                            expectedInput = rightKey;


                        }

                        //SUCCESS

                        if(expectedInput == leftKey){

                            expectedInput = rightKey;


                        }

                    }

                }


            }

            if(rightButton.isDown){

                phaserGame.input.keyboard.onUpCallback = function(e){

                    if(e.keyCode == rightButton){


                        //FIRST RUN

                        if(typeof expectedInput == 'undefined'){

                            expectedInput = leftKey;


                        }

                        //SUCCESS

                        if(expectedInput == rightKey){

                            expectedInput = leftKey;


                        }

                    }

                }


            }



            



            if(liptonBackgroundSpeed >= 0){



                runner.animations.stop('runForTea', true);



                gameState.runnerState = false;



                liptonBackgroundSpeed = 0;

            }



            if(liptonBackgroundSpeed < 0){



                runner.play('runForTea');



                gameState.runnerState = true;

            }







        }



    };







    setXSpeed = function (emitter, max) {



        emitter.setXSpeed(max - 20, max);



        emitter.forEachAlive(setParticleXSpeed, this, max);



    }







    setParticleXSpeed = function(particle, max) {



        particle.body.velocity.x = max - Math.floor(multiply(Math.random(), 30));



    }







    transformObjects = function(convertZone, _object){



        var rnd;



        colors = ["rosa", "azul", "rojo", "verde"];         







        rnd = Math.random()







        if (!multiplications[rnd]) {



            multiplications[rnd] = [];



        };



        if (!multiplications[rnd][4]) {



            multiplications[rnd][4] = rnd * 4;



        };



        extraname = colors[Math.floor(multiplications[rnd][4])];



        _object.animations.stop();



        _object.frameName = "regalo" + extraname + "0";



    }







    resize = function(_width, _height){



        phaserGame.camera.width = window.innerWidth;



        phaserGame.camera.height = window.innerHeight;



        background_6.cameraOffset.y = phaserGame.camera.height - background_6.height;



        liptonSky.cameraOffset.y = phaserGame.camera.height - liptonSky.height;



        phaserGame.camera.deadzone = new Phaser.Rectangle(0, 0, window.innerWidth - 238, window.innerHeight - 100);



        phaserGame.world.width = window.innerWidth;



        phaserGame.world.height = window.innerHeight;



    }







    updateScore = function(){



        if(typeof starCounter !== "undefined")



            starCounter.setText(gameState.starsAccumulated);



        if(typeof giftScore !== "undefined")



            giftScore.setText(gameState.giftsAccumulated);



        if(typeof score !== "undefined")



            score.setText(gameState.pointsAccumulated);



    }







    gameObject.loadGame = function(_ngScope){



        var phaserOptions, bootOptions;










        phaserOptions = function(){



        }







        phaserOptions.prototype = {



            resize: resize,



            preload: preload,



            create: create,



            update: update,



            render: render



        };







        bootOptions = function(){



        }







        bootOptions.prototype = {



          preload: function () {



            this.stage.backgroundColor =  "#0C555D";



            this.load.image("preloaderBar", "img/assets/preloaderBar.png");







          },



          create: function () {



            this.state.start('Game');







          }



        };







        //gamekey = key;







        ngScope = _ngScope;







        //gameSocket = socket;











        var gameMode = (navigator.platform.toUpperCase().indexOf('MAC')>=0)?Phaser.CANVAS:Phaser.AUTO;



            phaserGame = new Phaser.Game("100","100", gameMode, "canvas"/*, phaserOptions*/);



            phaserGame.state.add('Boot', bootOptions);



            phaserGame.state.add('Game', phaserOptions);



            phaserGame.state.start("Boot");



        //};







    }







    render = function(){



       //phaserGame.debug.text('timer' + this.game.time.totalElapsedSeconds(), 32, 32);



    }






    return gameObject;



})(this);
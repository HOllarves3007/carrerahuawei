(function(window, angular, moment){
// script.js
console = window.console || {log: function(){}};

    var key;

    var userTime;

    var baseURL = "https://lipton.cloudpolar.com/carreraLipton/";

    var loadScript = function(_name, _onload) {
        element = document.createElement("script");
        element.src = "js/" + _name + ".js";
        if (typeof _onload == "function") {
            element.onload = element.onerror =  element.onreadystatechange = _onload;
            document.body.appendChild(element);
        } else{
            //console.log("the _onload object must be a function");
        };
    }

    var loadGame = function(scope) {
        if(typeof gamePlay !== "object"){
            loadScript("gameplay", function(){
                gamePlay.loadGame(scope);
            });
        } else { gamePlay.loadGame(scope);
           
        }
    }

/*    var shareSiteFB = function($facebook){
                    $facebook.ui({
                        method: 'share',
                        href: "http://lipton.com/",
                    });
    }
*/
    var liptonApp = angular.module('liptonApp', ['ngRoute', 'ngFacebook', 'angular-canvasloader', 'ui.bootstrap','angularMoment']);

    // configure our routes
    liptonApp.config(function($routeProvider, $locationProvider) {

        $routeProvider

            .when('/', {
                templateUrl : 'pages/home.html',
                controller  : 'homeController'
            })

            .when('/home', {
                templateUrl : 'pages/home.html',
                controller  : 'homeController'
            })

            .when('/end', {
                templateUrl : 'pages/end.html',
                controller  : 'endController'
            })

            .when('/how2play', {
                 templateUrl : 'pages/how2play.html',
                 controller  : 'how2playController'
            })

            .when('/game', {
                 templateUrl : 'pages/game.html',
                 controller  : 'gameController'
            })

            .when('/nosotros', {
                 templateUrl : 'pages/nosotros.html',
                 controller  : 'nosotrosController'
            })
            
            .when('/records/', {
                 templateUrl : 'pages/records.html',
                 controller  : 'recordsController'
            })
            
            .when('/records/:from', {
                 templateUrl : 'pages/records.html',
                 controller  : 'recordsController'
            })
            ;

        // use the HTML5 History API
        //$locationProvider.html5Mode(true);
    }); 

    // create the controller and inject Angular's $scope
    liptonApp.controller('mainController', ['$scope', '$location', '$facebook', '$http', '$canvasLoader', function($scope, $location/*, socket*/, $facebook, $http, $canvasLoader) {
            //console.log("$id", $scope.$root.$id);
           /* $scope.sharesite = function(){
                shareSiteFB($facebook);                
            }

            $scope.shareresult = function(){
                $facebook.getLoginStatus().then(function(_loginInfo){
                    // TODO: VERIFICAR SI EL USUARIO JUGÓ YA
                    if(_loginInfo.status == "connected"){
                        $facebook.cachedApi("/me").then(function(_res){
                            $facebook.ui({
                                method: 'feed',
                                link: "http://loquenoteesperabas.com/",
                                caption: 'Ya atrapé mi regalo manejando el trineo de Santa. Juega tú también. ¡Hay premios!',
                                "picture": "http://loquenoteesperabas.com/img/sharerecords/" + _res.id + ".png"
                            });
                        });
                    } else {
                        shareSiteFB($facebook);
                    }
                });
            }*/

            $scope.$on("endGame", function(_evt, _object){
                //console.log('Catched!');
                //console.log("_object", _object);
                $canvasLoader.launch();
                $facebook.getLoginStatus().then(function(_loginInfo){
                    $facebook.cachedApi("/me").then(function(_me){
                        _me["score"] = _object.time;
                        userTime = _object.time;
                        $http
                            .post(baseURL + "scores/saveScore.json", _me)
                            .success(function(_res){
                                $canvasLoader.hide();
                            })
                            .error(function(_err){
                                $canvasLoader.hide();
                                //console.log(_err);
                            });

                    });
                });
            });

            ////console.log($scope);
            $scope.message = 'Everyone come and see how good I look!';
            $scope.valley = false;

            $scope.$on("ctrlLoad", function(_evt, _args){
                var element, head;
                //console.log(_args);
                if (_args == "about" || _args == "how2play" || _args == "records") {
                    //_evt.currentScope.valley = true;
                    _evt.currentScope.game = false;
                } else if(_args == "end") {
                    _evt.currentScope.valley = true;
                    _evt.currentScope.game = false;
                    if (!_evt.currentScope.total || !_evt.currentScope.gifts) {
                        //_evt.currentScope.total = 0;
                        //_evt.currentScope.gifts = 0;
                    }
                    //_evt.currentScope.gifts = 0;
                    //_evt.currentScope.total = 0;
                } else {
                    //_evt.currentScope.valley = false;
                    if (_args == "game") {
                        _evt.currentScope.game = true;
                        // Llamada a iniciar juego
                        if (typeof Phaser !== "object") {
                            loadScript("phaser.min", function(){
                                loadGame(_evt.targetScope);
                            });
                        } else {
                            loadGame(_evt.targetScope);
                        }
                    } else {
                        _evt.currentScope.game = false;
                    }
                }
            });
        }]);

    liptonApp.controller('homeController', ['$scope', '$location'/*, 'socket'*/, '$facebook', '$http', '$canvasLoader', function($scope, $location/*, socket*/, $facebook, $http, $canvasLoader, $routeParams) {
/*            $scope.fblogin = function(){
                $canvasLoader.launch();
                $facebook.login().then(function(_res){
                    if (_res.status == "connected") {
                        //console.log(_res.status);
                        $canvasLoader.hide();
                        $scope.play = true;
                        $location.path("/game");
                    } else{
                        //console.log(_res);
                    }
                }, function(err){
                    $canvasLoader.hide();
                    //console.log(err)
                });
            };*/

            $scope.tutorial = true;

            $(function(){
                $.fn.stopPapelillos(true);
            });

            $scope.fblogin = function(){
                $canvasLoader.launch();
                $facebook.login().then(function(_res){
                    if (_res.status == "connected") {
                        $facebook.cachedApi("/me?fields=name,email").then(function(_me){  
                            $http
                                .post(baseURL + "users/insertUser.json", _me)
                                .success(function(_res){
                                    $canvasLoader.hide();
                                    if (_res.result.status == "ok") {
                                        $location.path("/game");
                                    } else if (_res.result.status == "cantplay") {
                                        // Show cant play message
                                        window.alert("No tienes mas vidas");
                                    } else {
                                        // Show error message
                                    }
                                })
                                .error(function(_err){
                                    //console.log(_err);
                                });
                        });
                    } else{
                        $canvasLoader.hide();
                        window.alert("Por favor, acepta los permisos de Facebook para continuar");
                        //console.log(_res);
                    }
                }, function(err){
                    $canvasLoader.hide();
                    //console.log(err)
                });
            };
            $scope.$emit("ctrlLoad", "home");

    }]);

    liptonApp.controller('recordsController', ["$scope", "$http", "$location", "$facebook", "$routeParams", "$canvasLoader", function($scope, $http, $location, $facebook, $routeParams, $canvasLoader) {
        var getRecords, playAgain;

        /*getRecords = function(fbid){
            if (!fbid) {fbid = ""};

            $canvasLoader.launch();
            $facebook.getLoginStatus().then(function(_fbres){
            //$http.get("/getRecords" + fbid).success(function(_res){
                if (fbid) {
                    $scope.recordUser = _res.recordActive.pop();
                }
                $scope.records = _res.records;
                $canvasLoader.hide();
            }).error(function(_err){
                //console.log(_err);
            });
        };*/

        playAgain = function(){
            $facebook.getLoginStatus().then(function(response){
                if (response.status == "connected") {
                    $location.path("/sync/fb");
                } else {
                    $location.path("/sync/incognito");
                }
            });
        }

        $facebook.getLoginStatus().then(function(response){
            if (response.status == "connected") {
                $facebook.cachedApi("/me").then(function(_res){
                    //getRecords("/" + _res.id);
                });
            } else {
                getRecords();
            }
        });

        if ($routeParams.from == "game"){
            $scope.menu = false;
            $scope.play = true;
            $scope.playAgain = playAgain;
        } else {
            $scope.menu = true;
            $scope.play = false;
            $scope.playAgain = false;
        }
        $scope.$emit("ctrlLoad", "records");
    }]); 

    liptonApp.controller('syncactiveController', function($scope){
        $scope.$emit("ctrlLoad", "syncactive");
    }); 

    liptonApp.controller('endController', ["$scope", "$facebook", "$location","$http", "$canvasLoader", function($scope, $facebook, $location,$http, $canvasLoader) {
        $scope.invite = function(){
            $facebook.ui({
                method: 'apprequests',
                message: "Escápate con Lipton y diviértete mientras te alejas del caos de la ciudad"
            });
        }
        $scope.dare = function(){
            $facebook.ui({
                method: 'apprequests',
                message: "¡Soy el corredor más rápido! Escápate con Lipton tú también y trata de superarme"
            });
        }
        $scope.share = function(){
            $facebook.ui({
                method: 'feed',
                link: "https://lipton.cloudpolar.com/carreraLipton/redir",
                title: "Escápate con Lipton",
                picture: 'https://lipton.cloudpolar.com/carreraLipton/img/sharePicture.png',
                caption: 'Comparte con tus amigos de Facebook la diversión de "Escápate con Lipton" y rétalos a superarte'
            });
        }
        $scope.ask = function () {
            $facebook.ui({method: 'apprequests',
              message: 'Te tengo un regalo. Huawei',
              action_type: 'ask',
              max_recipients: 1,
              object_id: "434399263375548"
            }).then();
        }
        $canvasLoader.hide();
        $facebook.cachedApi("/me").then(function(_me){
            //_me["time"] = userTime || 1000;
            $http
                .post(baseURL + "users/getCondition.json", _me)
                .success(function(_res){
                    $canvasLoader.hide();
                    if (_res.result.status == "ok") {
                        x = parseInt(_me["score"], 'time');
                        var tempTime = moment.duration(x);
                        secs = tempTime.seconds();
                        mins = tempTime.minutes();
                        if(mins < 10) mins = "0" + mins.toString();
                        if(secs < 10) secs = "0" + secs.toString();
                        $scope.playerTime =  mins + ":" + secs;
                         if(_res.result.user.Score.rank==1) {
                            $scope.oneLife = false;
                            $scope.bestTime = true;
                            $scope.goodTry = false;
                            $scope.inviteFriends = false;
                            $scope.noLife = false;
                        } else {
                            $scope.oneLife = false;
                            $scope.bestTime = false;
                            $scope.goodTry = true;
                            $scope.inviteFriends = false;
                            $scope.noLife = false;

                            $scope.lifeLeft = _res.result.user.Life.quantity;

                            /*$scope.lifeCondition;
                            $scope.opportunity;
                            $scope.life;*/
                        }
                    } else {
                        // Show error message
                    }
                })
                .error(function(_err){
                    //console.log(_err);
                });
        });
        //console.log("endController $scope", $scope);
        $scope.$emit("ctrlLoad", "end");
    }]); 

    liptonApp.controller('how2playController', function($scope) {
        $scope.$emit("ctrlLoad", "how2play");
    });
    
    liptonApp.controller('gameController', function($scope) {

            $(function(){
                $.fn.stopPapelillos(true);
            });
        $scope.$emit("ctrlLoad", "game");
    });

    liptonApp.controller('nosotrosController', ["$scope", "$modal", function($scope, $modal) {
            $scope.$emit("ctrlLoad", "about");
            $scope.open = function(){
                var modalInstance = $modal.open({
                    controller: 'ModalInstanceCtrl',
                    template: '<div class="modal-body"><iframe width="560" height="315" src="//www.youtube.com/embed/fMBSWSrs-Vg" frameborder="0" allowfullscreen></iframe><button class="btn btn-default" ng-click="close()">Cerrar</button></div>'
                });
    
            }
    }]);

    liptonApp.controller('ModalInstanceCtrl', ["$scope", "$modal", "$modalInstance", function($scope, $modal, $modalInstance) {
        $scope.close = function () {
            $modalInstance.close();
        };
    }]);

    liptonApp.controller('endGameController',["$scope", "$location", function($scope, $location){

        

       $(function(){
            $.fn.activatePapelillos();
        });

    }]);

    liptonApp.controller('rankController', ["$scope", "$facebook", "$http", "$canvasLoader", function($scope, $facebook, $http, $canvasLoader){
        //console.log('RANK CONTROLLER IS HERE!');
        $scope.profilepic = {
            src: "img/sync/silueta.jpg",
            width: "56px",
            height: "56px"
        };
        $facebook.getLoginStatus().then(function(_loginInfo){
            if(_loginInfo.status == "connected"){
                $facebook.cachedApi("/me/picture?redirect=0&height=54&type=normal&width=54").then(function(_res){
                    if (!_res.data.is_silhouette) {
                        $scope.profilepic = {
                            src: _res.data.url,
                            width: _res.data.height + "px",
                            height: _res.data.width + "px"
                        };
                    };

                    $facebook.cachedApi("/me").then(function(_me){
                        $http
                            .post(baseURL + "scores/getScores.json", _me)
                            .success(function(_res){
                                $canvasLoader.hide();
                                if (_res.result.status == "ok") {
                                    $scope.scores = _res.result.scores;
                                    $scope.userScore = _res.result.userScore;
                                } else {
                                    // Show error message
                                    window.alert("Hubo un error, por favor intenta nuevametne");
                                }
                            })
                            .error(function(_err){
                                //console.log(_err);
                            });
                    });
                });
            }
        });

        $scope.userPosition = "#001";
        $scope.userName= "Henry Ollarves";
        $scope.firstOther="Alexis Rodriguez";
        $scope.firstOtherPosition="#002";
        $scope.secondOther="Batman";
        $scope.secondOtherPosition="#003";
        $scope.thirdOther="Manuel Azar";
        $scope.thirdOtherPosition="#004";
        $scope.fourthOther="Silvia Rojas";
        $scope.fourthOtherPosition="#005";
        $scope.fifthOther="Engels Perez";
        $scope.fifthOtherPosition="#005";
    }]);

    angular.module('liptonApp')
    .config( function( $facebookProvider ) {
      $facebookProvider.setAppId('129584644056690'); // Production
      //$facebookProvider.setAppId('129592227389265'); // Development
      $facebookProvider.setPermissions("email,public_profile");
      $facebookProvider.setCustomInit({xfbml: true,version    : 'v2.4'});
    });

})(this, angular, moment);


/*                        var mins = d.get("minutes");
                        var secs = d.get("seconds");*/
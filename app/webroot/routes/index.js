var express = require('express');
var router = express.Router();
var app = require('../app');
var MobileDetect = require('mobile-detect');
    


//variables de sesion 
var sess;
var desktop_client;
var mobile_client;

//Tendré un arreglo de objetos con las siguientes caracteristicas
//array[0].desktop_client
//array[0].mobile_client
var information = new Array();

/*funcion que retorna si un key es valido o no*/
function key_isValid(key){
	key = key.toUpperCase();
	tam = information.length;
	for (var i = 0; i < tam; i++) {
		if(key==information[i].desktop_client)
			return true;
	}
	return false;
}


getFbIdByKey = function(key){
	key = key.toUpperCase();
	tam = information.length;
	for (var i = 0; i < tam; i++) {
		if(key==information[i].desktop_client){
			console.log("FB "+information[i].fbid);
			return information[i].fbid;
		}
			
	}
	console.log("False ");
	return false;
}


/*function generateKey(){
	var text, possible;
    
    possible = "ABCDEFGHJKMNPQRSTUVWXYZ23456789";
    text = "";

    for( var i=0; i < 7; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
*/


/* GET home page. */
var homeListener = function(req, res) {
	/*var text;

	text  = generateKey();

	length_array = information.length;
	console.log("\n\rtam arreglo= "+length_array+"\n\r");

	console.log("\n\rkey= "+text+"\n\r");

	//desktop_client = text;
	desktop_client = "ANDRES";
	sess = req.session;
	sess.desktop = desktop_client;

	//creando el objeto a guardar en el arreglo
	var obj = new Object();
	obj.desktop_client = desktop_client;
	obj.mobile_client = null;
	
	information[length_array] = obj;*/
	md = new MobileDetect(req.headers['user-agent']);
	console.log( "Mobile "+md.mobile() );          // 'Sony'
	console.log( "Phone "+md.phone() );           // 'Sony'
	console.log( "Tablet "+md.tablet() );          // null
	console.log( "UserAgent "+md.userAgent() );       // 'Safari'
	console.log( "OS "+md.os() );              // 'AndroidOS'
	console.log( "is Iphone "+md.is('iPhone') );      // false
	console.log( "BOT "+md.is('bot') );         // false
	console.log( "webkit "+md.version('Webkit') );         // 534.3
	console.log( "build "+md.versionStr('Build') );       // '4.1.A.0.562'
	console.log( "playstation "+md.match('playstation|xbox') ); // false
	console.log("\r\n");
	if (md.mobile() != null || md.phone() != null || md.tablet() != null || md.is('iPhone') || md.os() == "AndroidOS"){
		res.render("landing");
	} else {
		res.render("inicio");	
	}
};

router.post('/', homeListener);
router.get('/', homeListener);

getRecords = function(req, res) {
	var score_table, my_score;
	if(typeof(req.param("fbid"))=== "undefined"){
		pool.query('Select MAX(tab.score) as score, CONCAT(tab.nombre, " ", tab.apellido) as user, "recordRow" as class, (@cnt := @cnt + 1) AS position FROM (SELECT ((gt.regalos * 250) + gt.score)as score, fb.firstname as nombre, fb.lastname as apellido, fb.fbid FROM gametimes as gt, games as g, fbusers as fb WHERE g.id=gt.idgame AND g.idfbuser =fb.fbid AND date(gt.requestime)=CURDATE()) as tab CROSS JOIN (SELECT @cnt := 0) AS dummy Group by tab.fbid ORDER BY score ASC', function(err, rows, fields) {
			if(!err) {
				console.log(rows);
				score_table = rows;
				res.json({records: rows});
			} else {
				console.log(err);
			    res.status(500).json({"err SIN FBID ": err});
			}
	    });
	}else{

		record_fbid = req.param("fbid");

		pool.query('SELECT MAX((gt.regalos * 250) + gt.score) as score, CONCAT(fb.firstname ," ", fb.lastname) as user, "recordRowActive" as class FROM gametimes as gt, games as g, fbusers as fb WHERE g.id=gt.idgame AND g.idfbuser =fb.fbid AND (date(gt.requestime)=CURDATE()) AND fb.fbid = ?',[record_fbid], function(err, rows, fields) {
			if(!err) {
				console.log(rows);
			    my_score = rows;
			    	pool.query('Select MAX(tab.score) as score, CONCAT(tab.nombre, " ", tab.apellido) as user, "recordRow" as class, (@cnt := @cnt + 1) AS position FROM (SELECT ((gt.regalos * 250) + gt.score)as score, fb.firstname as nombre, fb.lastname as apellido, fb.fbid FROM gametimes as gt, games as g, fbusers as fb WHERE g.id=gt.idgame AND g.idfbuser =fb.fbid AND date(gt.requestime)=CURDATE()) as tab CROSS JOIN (SELECT @cnt := 0) AS dummy Group by tab.fbid ORDER BY score ASC', function(err, rows, fields) {
			    		if(!err) {
			    			console.log(rows);
			    			score_table = rows;
						if(my_score[0].score == null)
							my_score = [];
							res.json({recordActive: my_score, records: score_table});
			    		} else {
			    			console.log(err);
			    		    res.status(500).json({"err SIN FBID ": err});
			    		}
			        });

			} else {
				console.log(err);
			    res.status(500).json({"err CON FBID MYSCORE": err});
			}
	    });
	}
	/*res.json({records: [
            {
                "user": "Lester Peña",
                "class": "recordRowActive",
                "score": 12500,
                "position": 500
            },
            {
                "user": "Lester Peña",
                "class": "recordRow",
                "score": 1250000,
                "position": 1
            },
            {
                "user": "Lester Peña",
                "class": "recordRow",
                "score": 1250000,
                "position": 2
            },
            {
                "user": "Lester Peña",
                "class": "recordRow",
                "score": 1250000,
                "position": 3
            },
            {
                "user": "Lester Peña",
                "class": "recordRow",
                "score": 1250000,
                "position": 4
            },
            {
                "user": "Lester Peña",
                "class": "recordRow",
                "score": 1250000,
                "position": 5
            },
            {
                "user": "Lester Peña",
                "class": "recordRow",
                "score": 1250000,
                "position": 6
            },
            {
                "user": "Lester Peña",
                "class": "recordRow",
                "score": 1250000,
                "position": 7
            },
            {
                "user": "Lester Peña",
                "class": "recordRow",
                "score": 1250000,
                "position": 8
            },
            {
                "user": "Lester Peña",
                "class": "recordRow",
                "score": 1250000,
                "position": 9
            },
            {
                "user": "Lester Peña",
                "class": "recordRow",
                "score": 1250000,
                "position": 10
            },
        ]});*/
};

router.get('/getRecords/', getRecords);
router.get('/getRecords/:fbid', getRecords);



router.post('/setRecords', function(req, res){
	var record_key = req.param("key");
	var record_points = req.param("points");
	var record_gifts = req.param("gifts");
	console.log("tengo key: "+record_key+" puntos: "+record_points+" regalos: "+record_gifts);
	
	var record_fbid = getFbIdByKey(record_key);
	console.log("el fb id es: "+record_fbid);

	if(record_fbid != false){
		pool.query('SELECT MAX(id) as game FROM games WHERE idfbuser = ? ',[record_fbid], function(err, rows, fields) {
		  if (err) throw err;
		  if(!err){
		  	queryNewGameTimes = "INSERT INTO gametimes SET ?";
		  	pool.query(queryNewGameTimes, 
			[{score: record_points, regalos: record_gifts, idgame: rows[0].game}],
			function(_queryerr, _result){
				if(!_queryerr){
					var spawn, phantomjs;
					spawn = require('child_process').spawn;
					phantomjs = spawn("phantomjs", ["topng.js", "http://loquenoteesperabas.com/getRecord/" + record_fbid, "public/img/sharerecords/" + record_fbid + ".png", "960", "720"])
					    .on("close", function(code, signal){
					        console.log(code, signal);
					        res.json({"result": "Ok", "url": "http://loquenoteesperabas.com/img/sharerecords/" + record_fbid + ".png"});
					    });
				}else{
					res.status(500).json({"result": "err Insertando gametimes", "msg": _queryerr, "data": _result});
					//res.status(500);
				}
			});

		  }else{
		  	res.status(500).json({"result": "err consultando game ID ", "msg": err});
		  }

		  console.log('The solution is: ', rows[0].game);
		});
	}
	
});

router.get("/recordByDate/:date",function(req,res){
	var response;
	var 	date = req.param("date");
	pool.query('Select MAX(tab.score) as score, CONCAT(tab.nombre, " ", tab.apellido) as user, "recordRow" as class, (@cnt := @cnt + 1) AS position,tab.profile_url, tab.pic_square FROM (SELECT ((gt.regalos * 250) + gt.score)as score, fb.firstname as nombre, fb.lastname as apellido, fb.fbid, fb.profile_url , fb.pic_square FROM gametimes as gt, games as g, fbusers as fb WHERE g.id=gt.idgame AND g.idfbuser =fb.fbid AND date(gt.requestime)=?) as tab CROSS JOIN (SELECT @cnt := 0) AS dummy Group by tab.fbid ORDER BY score ASC', [date], function(err, rows, fields) {
			if(!err) {
				console.log(rows);
				score_table = rows;
response = "";
response += "<table border='1'>";
	response += "<tr><td>position</td><td>user</td><td>pic</td><td>profile_url</td><td>rows[row].score</td></tr>\n";
for(row in rows){
	response += "<tr><td>" + rows[row].position + "</td><td>" + rows[row].user + "</td><td>" + rows[row].pic_square + "</td><td>" + rows[row].profile_url + "</td><td>" + rows[row].score + "</td></tr>\n";
}
//				res.json({records: rows});
response += "</table>";
res.send(response);
			} else {
				console.log(err);
			    res.status(500).json({"err SIN FBID ": err});
			}
	    });

})

router.get("/generateRecordImage/:fbid", function(req, res){
    var spawn, phantomjs, fbid;
    fbid = req.param("fbid");
    spawn = require('child_process').spawn;
    phantomjs = spawn("phantomjs", ["topng.js", "http://loquenoteesperabas.com/getRecord/" + fbid, "public/img/sharerecords/" + fbid + ".png", "960", "720"])
        .on("close", function(code, signal){
            console.log(code, signal);
            res.json({"url": "http://loquenoteesperabas.com/img/sharerecords/" + fbid + ".png"});
        });
});

router.get("/getRecord/:fbid", function(req, res){
	var record_fbid = req.param("fbid");
	pool.query('SELECT MAX((regalos * 250) + score) as score FROM gametimes LEFT JOIN games ON(gametimes.idgame=games.id) WHERE idfbuser=?',[record_fbid], function(err, rows, fields) {
		if(!err) {
			console.log(rows);
			score = rows[0].score;
			pool.query('SELECT regalos, idfbuser FROM gametimes LEFT JOIN games ON(gametimes.idgame=games.id) WHERE idfbuser=? AND ((regalos * 250) + score)=?',[record_fbid,score], function(err, rows, fields) {
				if(!err) {
					console.log(rows);
			    	res.render('getRecordTemplate', {"gifts": rows[0].regalos + " regalos", "points": score});
		    	} else {
					console.log(err);
				    res.status(500).json({"err": err});		
		    	}
			});
		} else {
			console.log(err);
		    res.status(500).json({"err": err});
		}
    });
});

router.post("/insertUser", function(req,res){
	var authResponse, me, status;

	authResponse = req.param("auth");
	me = req.param("me");

	if (me) {
		queryNewGame = "INSERT INTO games SET ?";
		pool.query(queryNewGame, 
		[{idfbuser: me.id.toString()}],
		function(_queryerr, _result){
			if (!_queryerr) {
				querystr = "INSERT INTO fbusers SET ? ON DUPLICATE KEY UPDATE ?, visits = visits + 1";
				query = pool.query(querystr, 
						[
							{
								fbid: me.id.toString(),
								firstname: me.first_name,
								lastname: me.last_name,
								accessToken: authResponse.accessToken,
								email: me.email,
								sex: me.gender,
								locale: me.locale,
								profile_url: me.link,
								pic_square: 'http://graph.facebook.com/'+me.id+'/picture?type=square',
								visits: 1
							}, 
							{accessToken: authResponse.accessToken}
						],
						function(_queryerr, _result){
							if (!_queryerr) {
								res.json({"result": "Ok"});
							} else{
								res.status(500).json({"result": "err", "msg": _queryerr, "data": _result});
							};					
						});
			} else{
				res.status(500).json({"result": "err create game", "msg": _queryerr, "data": _result});
			};					
		});
		
		//console.log(query);
	};
});

router.get('/getKey/:fbid', function(req, res) {
	var text;
	fbid = req.param("fbid");
	text  = generateKey();

    /*
	* FIN Generando cadena aleatoria
	*/
	//get tamano array
	length_array = information.length;
	console.log("\n\rtam arreglo= "+length_array+"\n\r");

	desktop_client = text;
//	desktop_client = "ANDRES";
	sess = req.session;
	sess.desktop = desktop_client;

	//creando el objeto a guardar en el arreglo
	var obj = new Object();
	obj.desktop_client = desktop_client;
	obj.fbid = fbid;
	obj.mobile_client = null;

	information[length_array] = obj;
	res.json({key: desktop_client});
});

router.get('/inicio', function(req, res) {
	//Preguntar si viene de un disp movil o no 
	//y dependiendo se muestra una pag u otra

	/*
	* Generando cadena aleatoria
	*/
	var text;

	text  = generateKey();

    /*
	* FIN Generando cadena aleatoria
	*/
	//get tamano array
	length_array = information.length;
	console.log("\n\rtam arreglo= "+length_array+"\n\r");

	//desktop_client = text;
	desktop_client = "ANDRES";
	sess = req.session;
	sess.desktop = desktop_client;

	//creando el objeto a guardar en el arreglo
	var obj = new Object();
	obj.desktop_client = desktop_client;
	obj.mobile_client = null;

	information[length_array] = obj;

	//console.log("Check if io exists in inicio:", typeof ioManager);
	res.render('inicioold', {key: desktop_client});

});

router.get("/movil", function(req, res){
	console.log("get movil");
	res.render("landing");
});

router.post("/movil", function(req, res){
	console.log("post movil");
	sess = req.session;
	sess.mobile=req.body.keymovil;
	mobile_client = sess.mobile;
	console.log("\r\nActivando el usuario: "+mobile_client);
	if(key_isValid(mobile_client)){
		//ioManager.emit('ready', mobile_client.toUpperCase(), desktop_client, "Aceptado");
		
		res.render('controlnew',{mobile: mobile_client.toUpperCase()});
	}else{
		res.render('fail',{desktop: desktop_client, mobile:mobile_client.toUpperCase()});
	}
	
});

router.get("/sincronizar", function(req, res){
	console.log("sincronizar GET");
	res.render("movil");
});

router.post("/carro", function(req, res){
	var current_key = req.body.clave;
	console.log("current_key: "+current_key);
	res.render("car", {key: current_key});
});

router.get("/variables", function(req, res){
	res.render('variables',{desktop: req.session.desktop, mobile:req.session.mobile});
});

router.get("/destroy", function(req, res){
	delete req.session.desktop;
	delete req.session.mobile;
	res.render("destruir");
});

router.get("/compartir", function(req, res){
	console.log("compartir called");
	res.render("compartir");
});


module.exports = router;

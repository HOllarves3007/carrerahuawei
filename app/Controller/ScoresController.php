<?php

App::uses('AppController', 'Controller');

/**
 * Scores Controller
 *
 * @property Score $Score
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */

class ScoresController extends AppController {



/**
 * Components
 *
 * @var array
 */

	public $components = array('Paginator', 'Session','RequestHandler');



	public $uses = ["Score", "User"];



	public function getScores()

	{

		$this->response->type('json');

		if($this->request->is("post")) {

			$scores = $this->Score->find("all", ["limit" => "5", "order" => ["Score.score" => "ASC"]]);

			$user = $this->User->findByFb_id($this->request->data["id"]);

			$userScore = [];

			if(isset($user["Score"])){

				$query = "SELECT getRank({$user["Score"]["user_id"]}) AS rank";

				$rank = $this->Score->query($query);

				$rank = $rank[0][0]["rank"];
				//$rank = 99;

				$userScore = $user["Score"];

				$userScore["rank"] = $rank;

				$userScore["name"] = $user["User"]["name"];

			}

			$result = ["status" => "ok", "scores" => $scores, "userScore" => $userScore];

		} else {

			$result = ["status" => "error"];

		}

		$this->set(compact("result"));

		$this->set(["_serialize" => ["result"]]);

	}



	public function saveScore()

	{

		$this->response->type('json');

		if($this->request->is("post")) {

			$user = $this->User->findByFb_id($this->request->data["id"]);

			//var_dump($user["Score"]);

			if(!isset($user["Score"]) || empty($user["Score"]) || empty($user["Score"]["score"])){
					$this->Score->create();
					$this->Score->save(["user_id" => $user["User"]["id"], "score" => $this->request->data["score"]]);
			}

			else {
			//	var_dump("im trying to update");

				if($this->request->data["score"] < $user["Score"]["score"]){
					$user["Score"]["score"] = $this->request->data["score"];
					$this->Score->save($user);
				}

			}

			$result = ["status" => "ok"];

		} else {

			$result = ["status" => "error"];

		}

		$this->set(compact("result"));

		$this->set(["_serialize" => ["result"]]);

	}



/**
 * index method
 *
 * @return void
 */

	public function index() {

		$this->Score->recursive = 0;

		$this->set('scores', $this->Paginator->paginate());

	}



/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function view($id = null) {

		if (!$this->Score->exists($id)) {

			throw new NotFoundException(__('Invalid score'));

		}

		$options = array('conditions' => array('Score.' . $this->Score->primaryKey => $id));

		$this->set('score', $this->Score->find('first', $options));

	}



/**
 * add method
 *
 * @return void
 */

	public function add() {

		if ($this->request->is('post')) {

			$this->Score->create();

			if ($this->Score->save($this->request->data)) {

				$this->Session->setFlash(__('The score has been saved.'));

				return $this->redirect(array('action' => 'index'));

			} else {

				$this->Session->setFlash(__('The score could not be saved. Please, try again.'));

			}

		}

		$users = $this->Score->User->find('list');

		$this->set(compact('users'));

	}



/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function edit($id = null) {

		if (!$this->Score->exists($id)) {

			throw new NotFoundException(__('Invalid score'));

		}

		if ($this->request->is(array('post', 'put'))) {

			if ($this->Score->save($this->request->data)) {

				$this->Session->setFlash(__('The score has been saved.'));

				return $this->redirect(array('action' => 'index'));

			} else {

				$this->Session->setFlash(__('The score could not be saved. Please, try again.'));

			}

		} else {

			$options = array('conditions' => array('Score.' . $this->Score->primaryKey => $id));

			$this->request->data = $this->Score->find('first', $options);

		}

		$users = $this->Score->User->find('list');

		$this->set(compact('users'));

	}



/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function delete($id = null) {

		$this->Score->id = $id;

		if (!$this->Score->exists()) {

			throw new NotFoundException(__('Invalid score'));

		}

		$this->request->allowMethod('post', 'delete');

		if ($this->Score->delete()) {

			$this->Session->setFlash(__('The score has been deleted.'));

		} else {

			$this->Session->setFlash(__('The score could not be deleted. Please, try again.'));

		}

		return $this->redirect(array('action' => 'index'));

	}

}


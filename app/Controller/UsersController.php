<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */

class UsersController extends AppController {



/**
 * Components
 *
 * @var array
 */

	public $components = array('Paginator', 'Session','RequestHandler');



	public $uses = ["User", "Life"];



	public function getCondition()

	{

		$this->response->type('json');

		if($this->request->is("post")) {

			$user = $this->User->find("first", ["conditions" => ["User.fb_id" => $this->request->data["id"]]]);

			$query = "SELECT getRank({$user["Score"]["user_id"]}) AS rank";

			$rank = $this->User->query($query);

			if($rank[0][0]["rank"] == 1 && $user["Score"]["score"] != $this->request->data["score"]){
				$rank = 2;
			} else {
				$rank = $rank[0][0]["rank"];
			}

			$user["Score"]["rank"] = $rank;

			//$user["Score"]["rank"] = 99;

			$result = ["status" => "ok", "user" => $user];

		} else {

			$result = ["status" => "error", "message" => "Request is not post"];

		}		

		$this->set(compact("result"));;

		$this->set(["_serialize" => ["result"]]);

	}



	public function insertUser()

	{

		$this->response->type('json');

		if($this->request->is("post")) {

			$count = $this->User->find("count", ["conditions" => ["User.fb_id" => $this->request->data["id"]]]);

			if($count > 0) {

				$user = $this->User->find("first", ["conditions" => ["User.fb_id" => $this->request->data["id"]]]);

				if($user["Life"]["quantity"] > 0){

					$this->Life->id = $user["Life"]["id"];

					$user["Life"]["quantity"] -= 1;

					$this->Life->save($user);

					$result = ["status" => "ok"];

				} else {

					$result = ["status" => "ok"];

				}

			} else {

				$this->User->create();

				$newUser = [

					"User" => [

						"name" => $this->request->data["name"],

						"fb_id" => $this->request->data["id"],

						"email" => $this->request->data["email"],

					]

				];

				if($this->User->save($newUser)){

					$result = ["status" => "ok"];

				} else {

					$result = ["status" => "error", "message" => "Couldnt save"];

				}



			}

		} else {

			$result = ["status" => "error", "message" => "Request is not post"];

		}		

		$this->set(compact("result"));;

		$this->set(["_serialize" => ["result"]]);

	}



/**
 * index method
 *
 * @return void
 */

	public function index() {

		$this->User->recursive = 0;

		$this->set('users', $this->Paginator->paginate());

	}



/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function view($id = null) {

		if (!$this->User->exists($id)) {

			throw new NotFoundException(__('Invalid user'));

		}

		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));

		$this->set('user', $this->User->find('first', $options));

	}



/**
 * add method
 *
 * @return void
 */

	public function add() {

		if ($this->request->is('post')) {

			$this->User->create();

			if ($this->User->save($this->request->data)) {

				$this->Session->setFlash(__('The user has been saved.'));

				return $this->redirect(array('action' => 'index'));

			} else {

				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));

			}

		}

	}



/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function edit($id = null) {

		if (!$this->User->exists($id)) {

			throw new NotFoundException(__('Invalid user'));

		}

		if ($this->request->is(array('post', 'put'))) {

			if ($this->User->save($this->request->data)) {

				$this->Session->setFlash(__('The user has been saved.'));

				return $this->redirect(array('action' => 'index'));

			} else {

				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));

			}

		} else {

			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));

			$this->request->data = $this->User->find('first', $options);

		}

	}



/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function delete($id = null) {

		$this->User->id = $id;

		if (!$this->User->exists()) {

			throw new NotFoundException(__('Invalid user'));

		}

		$this->request->allowMethod('post', 'delete');

		if ($this->User->delete()) {

			$this->Session->setFlash(__('The user has been deleted.'));

		} else {

			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));

		}

		return $this->redirect(array('action' => 'index'));

	}

}


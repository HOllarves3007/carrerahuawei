<?php
App::uses('AppController', 'Controller');
/**
 * Lives Controller
 *
 * @property Life $Life
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class LivesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Life->recursive = 0;
		$this->set('lives', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Life->exists($id)) {
			throw new NotFoundException(__('Invalid life'));
		}
		$options = array('conditions' => array('Life.' . $this->Life->primaryKey => $id));
		$this->set('life', $this->Life->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Life->create();
			if ($this->Life->save($this->request->data)) {
				$this->Session->setFlash(__('The life has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The life could not be saved. Please, try again.'));
			}
		}
		$users = $this->Life->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Life->exists($id)) {
			throw new NotFoundException(__('Invalid life'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Life->save($this->request->data)) {
				$this->Session->setFlash(__('The life has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The life could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Life.' . $this->Life->primaryKey => $id));
			$this->request->data = $this->Life->find('first', $options);
		}
		$users = $this->Life->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Life->id = $id;
		if (!$this->Life->exists()) {
			throw new NotFoundException(__('Invalid life'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Life->delete()) {
			$this->Session->setFlash(__('The life has been deleted.'));
		} else {
			$this->Session->setFlash(__('The life could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
